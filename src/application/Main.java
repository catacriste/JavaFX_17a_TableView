package application;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;


public class Main extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}
	
	
	@Override
	public void start(Stage primaryStage) {
		try {
			TableView<Aluno> tableAluno = new TableView<>();
			TableColumn<Aluno, String> colunaNome = new TableColumn<>("Nome");
			
			colunaNome.setMinWidth(200);
			colunaNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
			
			TableColumn<Aluno, String> colunaNumero = new TableColumn<>("Numero");
			colunaNumero.setMinWidth(20);
			colunaNumero.setCellValueFactory(new PropertyValueFactory<>("nTurma"));
			
			tableAluno.getColumns().addAll(colunaNumero,colunaNome);
			
			tableAluno.setItems( carregaListAluno() );
			
			StackPane layout = new StackPane();
			layout.setPadding(new Insets(20,20,20,20));
			layout.getChildren().add(tableAluno);
			
			
			
			BorderPane root = new BorderPane();
			Scene scene = new Scene(layout,400,400);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private ObservableList<Aluno> carregaListAluno()
	{
		ObservableList<Aluno> listaAluno = FXCollections.observableArrayList();
		
		listaAluno.add(new Aluno(0, "A.Ferraz"));
		listaAluno.add(new Aluno(1, "Bruno Coimbra"));
		listaAluno.add(new Aluno(2, "Catalin Criste"));
		listaAluno.add(new Aluno(3, "David Sousa"));
		listaAluno.add(new Aluno(6, "Jo�o Ventura"));
		listaAluno.add(new Aluno(7, "Jorge Silva"));
		listaAluno.add(new Aluno(8, "Marcelo"));
		listaAluno.add(new Aluno(9, "Mariana"));
		listaAluno.add(new Aluno(10, "Patricia"));
		listaAluno.add(new Aluno(11, "Ricardo"));
		listaAluno.add(new Aluno(12, "Ruben"));
		listaAluno.add(new Aluno(14, "Rui"));
		listaAluno.add(new Aluno(15, "Samuel"));
		
		return listaAluno;
	}
	
	
}
